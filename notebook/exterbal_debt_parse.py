import os
import pandas as pd
from openpyxl import load_workbook
import xlrd

columns = ["Категория долга", "Дата", "Сумма в долл" , "Сумма в евро"]
dataframe = pd.DataFrame(columns=columns)

for year in range(2011, 2022):
    for month in range(1, 13):
        month_str = str(month) if month > 9 else '0' + str(month)

        if not os.path.exists(f'data/STR_WEB_{year}_01.{month_str}.{year}.xlsx'):
            continue

        try:
            wb = load_workbook(f'data/STR_WEB_{year}_01.{month_str}.{year}.xlsx', data_only=True)
            # Get a sheet by name
            sheet = wb.get_sheet_by_name(wb.get_sheet_names()[0])

            for i in range(1, 50):
                name = str(sheet[f"A{i}"].value).replace("\n", " ")
                if name is not None:
                    if "задолженность" in name.lower() or "гарантии" in name.lower():
                        values = [name, f"01.{month_str}.{year}", float(sheet[f"B{i}"].value) * 10e6,
                                  float(sheet[f"C{i}"].value) * 10e6]
                        dataframe = dataframe.append(dict(zip(columns, values)), ignore_index=True)
        except:
            wb = xlrd.open_workbook(f'data/STR_WEB_{year}_01.{month_str}.{year}.xlsx',formatting_info=True)
            sheet = wb.sheet_by_index(0)

            for i in range(1, sheet.nrows):
                name = str(sheet.cell(i,0).value).replace("\n", " ")
                if name is not None:
                    if "задолженность" in name.lower() or "гарантии" in name.lower():
                        values = [name, f"01.{month_str}.{year}", float(sheet.cell(i,1).value) * 10e6,
                                  float(sheet.cell(i,2).value) * 10e6]
                        dataframe = dataframe.append(dict(zip(columns, values)), ignore_index=True)

print(dataframe)
dataframe.to_csv("External.csv", index=False)