import pandas as pd
df = pd.read_excel('/media/ubuntu/DATA/Projects/Personal/audit-company/data/WEOOct2020all.xlsx')
df = df.drop(['WEO Country Code', 'Scale', 'Subject Descriptor', 'Subject Notes', 'Units', 'Estimates Start After', 'Country/Series-specific Notes'], axis=1)

codes = {
    "NGDPD": "ВВП",
    "NGDPDPC": "ВВП на душу населения",
    "GGXWDG_NGDP": "% гос. долга от ВВП",
}

df = df[df['WEO Subject Code'].isin(list(codes.keys()))]
df['WEO Subject Code'] = df['WEO Subject Code'].map(codes)

df.columns = ['Код страны', 'Показатель', 'Страна'] + df.columns[3:].tolist()
for column in df.columns[3:].tolist():
    df[column].loc[df['Показатель'] == 'ВВП'] = df[column].loc[df['Показатель'] == 'ВВП'] * 10e9


df = df.set_index(['Код страны', 'Показатель', 'Страна'])
df = df.stack()

df = df.reset_index()

df.columns = ['Код страны', 'Показатель', 'Страна', 'Год', 'Значение']
df = df.set_index(['Код страны', 'Страна', 'Год','Показатель',])
df = df.unstack('Показатель')
df.columns = df.columns.droplevel()
df = df.reset_index()
print(df)

df.to_csv('/media/ubuntu/DATA/Projects/Personal/audit-company/GDP_GDP_per_capita_debt_v2.csv', index=False)