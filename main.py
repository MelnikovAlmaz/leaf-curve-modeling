import os

import cv2
import numpy
import seaborn
import matplotlib.pyplot as plt
from geomdl import BSpline, exchange, utilities, fitting
from geomdl.visualization import VisMPL

from src.data.image_process import load_image, object_contour_detection, contour_pixel_coordinates, normalize_position, \
    cart2pol, normalize_variable, vector_extremum_indexes, rotate_curve, rotate
from src.features import control_points_from_data_points

if __name__ == "__main__":
    base_dir = "/media/ubuntu/DATA/Projects/Personal/leaf-curve-modeling/"

    image_dir = base_dir + "data/100_leaves_plant_species/data/Alnus_Cordata/"

    for image_file in os.listdir(image_dir):
        image_path = image_dir + image_file

        image = load_image(image_path)

        contour_image = object_contour_detection(image)

        coordinates = contour_pixel_coordinates(contour_image)

        center_coordinates = normalize_position(coordinates)

        (rho, phi) = cart2pol(center_coordinates)
        rho, max_rho = normalize_variable(rho)
        index_order = numpy.argsort(phi)

        phi = phi[index_order]
        rho = rho[index_order]
        center_coordinates = center_coordinates[index_order]

        plt.plot(phi, rho)
        plt.show()

        # Calculate control points
        segment_evaluated_point_list, segment_control_point_list = control_points_from_data_points(
            data_points=center_coordinates,
            segment_number=10,
            control_point_per_segment=10,
            spline_degree=4
        )

        segment_evaluated_point_list = numpy.reshape(
            segment_evaluated_point_list,
            (segment_evaluated_point_list.shape[0] * segment_evaluated_point_list.shape[1], segment_evaluated_point_list.shape[2])
        )

        segment_control_point_list = numpy.reshape(
            segment_control_point_list,
            (segment_control_point_list.shape[0] * segment_control_point_list.shape[1],
             segment_control_point_list.shape[2])
        )

        (control_rho, control_phi) = cart2pol(segment_control_point_list)
        control_rho = control_rho / max_rho

        plt.plot(center_coordinates[:, 0], center_coordinates[:, 1])
        plt.plot(segment_evaluated_point_list[:, 0], segment_evaluated_point_list[:, 1])
        plt.plot(segment_control_point_list[:, 0], segment_control_point_list[:, 1], "x")
        plt.show()

        cv2.imshow("Leaf original", image)
        cv2.imshow("Leaf contour", contour_image)
        cv2.waitKey()
