import argparse
import os
import pickle

import cv2
import numpy
import yaml

from src.data.image_process import object_contour_detection, contour_pixel_coordinates, cart2pol, normalize_position
from src.data.vector_utils import interpolate_vector
from src.utils.logging import get_logger


def contour_coordinates(config_path: str) -> None:
    """
    Detect contour on image.
    Args:
        config_path: path to yaml file with params

    Returns: None

    """
    # Params
    config = yaml.safe_load(open(config_path, 'r'))
    log_level = config['base']['log_level']
    project_path = config['base']['project_path']
    coordinates_path = os.path.join(project_path, config['contour_coordinates']['coordinates_path'])
    target_path = os.path.join(project_path, config['contour_coordinates']['target_path'])
    dataset_dir = os.path.join(project_path, config['contour_coordinates']['dataset_dir'])
    point_count = config['contour_coordinates']['point_count']

    logger = get_logger("CONTOUR_COORDINATES", log_level)

    logger.info("Extract contour coordinates")

    contour_coordinate_list = []
    target_list = []
    for class_name in os.listdir(dataset_dir):
        for image_name in os.listdir(os.path.join(dataset_dir, class_name)):
            src_image_path = os.path.join(dataset_dir, class_name, image_name)

            contour_image = cv2.imread(src_image_path)
            contour_image = cv2.cvtColor(contour_image, cv2.COLOR_BGR2GRAY)
            contour_image = numpy.where(contour_image > 128, 255, 0).astype(numpy.uint8)

            contour_coordinates = contour_pixel_coordinates(contour_image)
            # Rearrange coordinates by angle
            (rho, phi) = cart2pol(normalize_position(contour_coordinates))
            index_order = numpy.argsort(phi)
            contour_coordinates = numpy.vstack([contour_coordinates[index_order[0]:], contour_coordinates[:index_order[0]]])
            # Interpolate coordinates
            contour_coordinates = interpolate_vector(contour_coordinates, point_count)

            contour_coordinate_list.append(contour_coordinates)

            target_list.append(class_name)

    logger.info("Save coordinates")
    with open(coordinates_path, 'wb') as file:
        pickle.dump(contour_coordinate_list, file)
    logger.info("Save coordinates target")
    with open(target_path, 'wb') as file:
        pickle.dump(target_list, file)
        
    logger.info("Finish extract contour coordinate")


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--config', required=True, help='path to yaml file with params')

    args = parser.parse_args()
    contour_coordinates(config_path=args.config)
