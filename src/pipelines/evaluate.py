import argparse
import json
import os
import pickle
import mlflow
import numpy
import yaml
from sklearn.model_selection import cross_validate
from sklearn.neighbors import KNeighborsClassifier
from tqdm import tqdm

from src.features.ccod_descriptor import center_coordinate_distance_descriptor
from src.features.fourier_features import coordinates_fourier_descriptor, polar_coordinates_fourier_descriptor
from src.features.spline_features import spline_knot_vector_descriptor
from src.features.wavelet_features import coordinates_wavelet_descriptor, polar_coordinates_wavelet_descriptor
from src.utils.logging import get_logger


def evaluate(config_path: str) -> None:
    """
    Detect contour on image.
    Args:
        config_path: path to yaml file with params

    Returns: None

    """
    # Params
    config = yaml.safe_load(open(config_path, 'r'))
    log_level = config['base']['log_level']
    project_path = config['base']['project_path']

    coordinates_path = os.path.join(project_path, config['evaluate']['coordinates_path'])
    target_path = os.path.join(project_path, config['evaluate']['target_path'])
    descriptor_name = config['evaluate']['descriptor_name']
    descriptor_shapes = config['evaluate']['descriptor_shapes']
    n_neighbors = config['evaluate']['n_neighbors']
    cv_folds = config['evaluate']['cv_folds']
    result_path = os.path.join(project_path, config['evaluate']['result_path'])
    experiment_id = config['evaluate']['experiment_id']

    logger = get_logger("EVALUATE", log_level)
    logger.info("Evaluate")

    logger.info("Load coordinates")
    with open(coordinates_path, 'rb') as file:
        contour_coordinate_list = pickle.load(file)
    logger.info("Load coordinates target")
    with open(target_path, 'rb') as file:
        target_list = pickle.load(file)

    results = {}
    for descriptor_shape in descriptor_shapes:
        logger.info(f"Prepare features for descriptor shape {descriptor_shape}")
        results[f"descriptor_shape_{descriptor_shape}"] = {}

        descriptor_list = []
        for coordinates in tqdm(contour_coordinate_list):
            if descriptor_name == "ccod":
                descriptor = center_coordinate_distance_descriptor(coordinates, descriptor_shape)
            if descriptor_name == "fourier_coordinates":
                descriptor = coordinates_fourier_descriptor(coordinates, descriptor_shape)
            if descriptor_name == "wavelet_coordinates":
                descriptor = coordinates_wavelet_descriptor(coordinates, descriptor_shape)
            if descriptor_name == "fourier_polar_coordinates":
                descriptor = polar_coordinates_fourier_descriptor(coordinates, descriptor_shape)
            if descriptor_name == "wavelet_polar_coordinates":
                descriptor = polar_coordinates_wavelet_descriptor(coordinates, descriptor_shape)
            if descriptor_name == "ccod_fourier_coordinates":
                ccod_descriptor = center_coordinate_distance_descriptor(coordinates, descriptor_shape)
                fourier_descriptor = coordinates_fourier_descriptor(coordinates, descriptor_shape)
                descriptor = numpy.hstack([ccod_descriptor, fourier_descriptor])
            if descriptor_name == "ccod_wavelet_coordinates":
                ccod_descriptor = center_coordinate_distance_descriptor(coordinates, descriptor_shape)
                wavelet_descriptor = coordinates_wavelet_descriptor(coordinates, descriptor_shape)
                descriptor = numpy.hstack([ccod_descriptor, wavelet_descriptor])
            if descriptor_name == "fourier_wavelet_coordinates":
                fourier_descriptor = coordinates_fourier_descriptor(coordinates, descriptor_shape)
                wavelet_descriptor = coordinates_wavelet_descriptor(coordinates, descriptor_shape)
                descriptor = numpy.hstack([fourier_descriptor, wavelet_descriptor])
            if descriptor_name == "ccod_fourier_wavelet_coordinates":
                ccod_descriptor = center_coordinate_distance_descriptor(coordinates, descriptor_shape)
                fourier_descriptor = coordinates_fourier_descriptor(coordinates, descriptor_shape)
                wavelet_descriptor = coordinates_wavelet_descriptor(coordinates, descriptor_shape)
                descriptor = numpy.hstack([ccod_descriptor, fourier_descriptor, wavelet_descriptor])
            descriptor_list.append(descriptor)

        logger.info(f"Cross validation for descriptor shape {descriptor_shape}")
        model = KNeighborsClassifier(n_neighbors=n_neighbors)
        scores = cross_validate(model, descriptor_list, target_list, cv=cv_folds, scoring=['f1_macro', 'accuracy'])

        # Collect results
        results[f"descriptor_shape_{descriptor_shape}"]['accuracy_mean'] = scores['test_accuracy'].mean()
        results[f"descriptor_shape_{descriptor_shape}"]['accuracy_var'] = scores['test_accuracy'].var()
        results[f"descriptor_shape_{descriptor_shape}"]['f1_macro_mean'] = scores['test_f1_macro'].mean()
        results[f"descriptor_shape_{descriptor_shape}"]['f1_macro_var'] = scores['test_f1_macro'].var()

        # Trace metrics at mlflow
        with mlflow.start_run(experiment_id=experiment_id):
            mlflow.log_param("descriptor_shape", descriptor_shape)
            mlflow.log_param("descriptor_name", descriptor_name)

            mlflow.log_metric("accuracy_mean", scores['test_accuracy'].mean())
            mlflow.log_metric("accuracy_var", scores['test_accuracy'].var())
            mlflow.log_metric("f1_macro_mean", scores['test_f1_macro'].mean())
            mlflow.log_metric("f1_macro_var", scores['test_f1_macro'].var())

    logger.info("Save results")
    metric_filename = f"{descriptor_name}.json"
    with open(os.path.join(result_path, metric_filename), 'w') as file:
        file.write(json.dumps(results, indent=experiment_id))

    logger.info("Finish evaluate")


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--config', default="/media/ubuntu/DATA/Projects/Personal/leaf-curve-modeling/params.yaml", help='path to yaml file with params')

    args = parser.parse_args()
    evaluate(config_path=args.config)
