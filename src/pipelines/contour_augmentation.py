import argparse
import os
import pickle
import random

import numpy
import yaml
import matplotlib.pyplot as plt

from src.data.vector_utils import rotate
from src.utils.logging import get_logger


def contour_augmentation(config_path: str) -> None:
    """
    Detect contour on image.
    Args:
        config_path: path to yaml file with params

    Returns: None

    """
    # Params
    config = yaml.safe_load(open(config_path, 'r'))
    log_level = config['base']['log_level']
    project_path = config['base']['project_path']
    random_state = config['base']['random_state']

    coordinates_path = os.path.join(project_path, config['contour_augmentation']['coordinates_path'])
    target_path = os.path.join(project_path, config['contour_augmentation']['target_path'])

    shift_coordinates_path = os.path.join(project_path, config['contour_augmentation']['shift_coordinates_path'])
    shift_target_path = os.path.join(project_path, config['contour_augmentation']['shift_target_path'])

    rotate_coordinates_path = os.path.join(project_path, config['contour_augmentation']['rotate_coordinates_path'])
    rotate_target_path = os.path.join(project_path, config['contour_augmentation']['rotate_target_path'])

    scale_coordinates_path = os.path.join(project_path, config['contour_augmentation']['scale_coordinates_path'])
    scale_target_path = os.path.join(project_path, config['contour_augmentation']['scale_target_path'])

    shift_delta = numpy.array(config['contour_augmentation']['shift_delta'])
    rotate_angle = config['contour_augmentation']['rotate_angle']
    scale_delta = config['contour_augmentation']['scale_delta']
    augmentation_count = config['contour_augmentation']['augmentation_count']

    logger = get_logger("CONTOUR_AUGMENTATION", log_level)
    logger.info("Augment contour coordinates")

    logger.debug("Set seed")
    random.seed = random_state
    numpy.random.seed(random_state)

    logger.info("Load coordinates")
    with open(coordinates_path, 'rb') as file:
        contour_coordinate_list = pickle.load(file)
    logger.info("Load coordinates target")
    with open(target_path, 'rb') as file:
        target_list = pickle.load(file)

    logger.info("Shift augmentation")
    shift_coordinate_list = []
    shift_target_list = []
    for index, coordinates in enumerate(contour_coordinate_list):
        max_y, max_x = max(coordinates[:, 0]) + 1, max(coordinates[:, 1]) + 1
        shift_y, shift_x = shift_delta * max_y, shift_delta * max_x
        shift_y, shift_x = shift_y.astype(int), shift_x.astype(int)
        for i in range(augmentation_count):
            delta_y = numpy.random.randint(low=shift_y[0], high=shift_y[1], size=[1])[0]
            delta_x = numpy.random.randint(low=shift_x[0], high=shift_x[1], size=[1])[0]
            # Transform
            transformed_coordinates = coordinates.copy()
            transformed_coordinates[:, 0] += delta_y
            transformed_coordinates[:, 1] += delta_x

            shift_coordinate_list.append(transformed_coordinates)
            shift_target_list.append(target_list[index])

    logger.info("Rotate augmentation")
    rotate_coordinate_list = []
    rotate_target_list = []
    for index, coordinates in enumerate(contour_coordinate_list):
        for i in range(augmentation_count):
            delta_rotate = numpy.random.randint(low=-rotate_angle, high=rotate_angle, size=[1])[0]
            # Transform
            transformed_coordinates = coordinates.copy()
            transformed_coordinates_origin = numpy.array([transformed_coordinates[:, 0].mean(), transformed_coordinates[:, 1].mean()])
            transformed_coordinates = rotate(transformed_coordinates, transformed_coordinates_origin, delta_rotate)

            rotate_coordinate_list.append(transformed_coordinates)
            rotate_target_list.append(target_list[index])

    logger.info("Scale augmentation")
    scale_coordinate_list = []
    scale_target_list = []
    for index, coordinates in enumerate(contour_coordinate_list):
        for i in range(augmentation_count):
            scale = 1 + numpy.random.random(size=[1])[0] * scale_delta * 2 - scale_delta
            # Transform
            transformed_coordinates = coordinates.copy()
            transformed_coordinates = transformed_coordinates * scale
            # Match origins
            coordinates_origin = numpy.array(
                [coordinates[:, 0].mean(), coordinates[:, 1].mean()])
            transformed_coordinates_origin = numpy.array(
                [transformed_coordinates[:, 0].mean(), transformed_coordinates[:, 1].mean()])
            origin_diff = coordinates_origin - transformed_coordinates_origin
            transformed_coordinates += origin_diff

            scale_coordinate_list.append(transformed_coordinates)
            scale_target_list.append(target_list[index])

    logger.info("Save shift coordinates")
    with open(shift_coordinates_path, 'wb') as file:
        pickle.dump(shift_coordinate_list, file)
    logger.info("Save shift coordinates target")
    with open(shift_target_path, 'wb') as file:
        pickle.dump(shift_target_list, file)

    logger.info("Save rotate coordinates")
    with open(rotate_coordinates_path, 'wb') as file:
        pickle.dump(rotate_coordinate_list, file)
    logger.info("Save rotate coordinates target")
    with open(rotate_target_path, 'wb') as file:
        pickle.dump(rotate_target_list, file)

    logger.info("Save scale coordinates")
    with open(scale_coordinates_path, 'wb') as file:
        pickle.dump(scale_coordinate_list, file)
    logger.info("Save scale coordinates target")
    with open(scale_target_path, 'wb') as file:
        pickle.dump(scale_target_list, file)

    logger.info("Finish augment contour coordinates")


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--config', required=True, help='path to yaml file with params')

    args = parser.parse_args()
    contour_augmentation(config_path=args.config)
