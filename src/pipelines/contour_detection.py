import argparse
import os

import cv2
import yaml

from src.data.image_process import object_contour_detection
from src.utils.logging import get_logger


def contour_detection(config_path: str) -> None:
    """
    Detect contour on image.
    Args:
        config_path: path to yaml file with params

    Returns: None

    """
    # Params
    config = yaml.safe_load(open(config_path, 'r'))
    log_level = config['base']['log_level']
    project_path = config['base']['project_path']
    target_dir = os.path.join(project_path, config['contour_detection']['target_dir'])
    dataset_dir = os.path.join(project_path, config['contour_detection']['dataset_dir'])
    remove_background = config['contour_detection']['remove_background']

    logger = get_logger("CONTOUR_DETECTION", log_level)

    logger.info("Detect contours")
    for class_name in os.listdir(dataset_dir):
        for image_name in os.listdir(os.path.join(dataset_dir, class_name)):
            src_image_path = os.path.join(dataset_dir, class_name, image_name)
            dst_image_path = os.path.join(target_dir, class_name, image_name)

            os.makedirs(os.path.join(target_dir, class_name), exist_ok=True)

            image = cv2.imread(src_image_path)
            contour_image = object_contour_detection(image, remove_bg=remove_background)
            cv2.imwrite(dst_image_path, contour_image)

    logger.info("Finish detect contours")


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--config', required=True, help='path to yaml file with params')

    args = parser.parse_args()
    contour_detection(config_path=args.config)
