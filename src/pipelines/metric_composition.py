import argparse
import json
import os
import yaml

import pandas

from src.utils.logging import get_logger


def metric_composition(config_path: str) -> None:
    """
    Detect contour on image.
    Args:
        config_path: path to yaml file with params

    Returns: None

    """
    # Params
    config = yaml.safe_load(open(config_path, 'r'))
    log_level = config['base']['log_level']
    project_path = config['base']['project_path']

    cross_validate_path = os.path.join(project_path, config['metric_composition']['cross_validate_path'])
    invariance_path = os.path.join(project_path, config['metric_composition']['invariance_path'])
    metric_path = os.path.join(project_path, config['metric_composition']['metric_path'])

    descriptor_names = config['metric_composition']['descriptor_names']
    descriptor_shapes = config['metric_composition']['descriptor_shapes']

    logger = get_logger("METRIC_COMPOSITION", log_level)
    logger.info("Metric composition")

    metrics = []
    for descriptor_name in descriptor_names:
        metric_filename = f"{descriptor_name}.json"
        descriptor_cross_validate_results = None
        descriptor_invariance_results = None

        with open(os.path.join(cross_validate_path, metric_filename), 'r') as file:
            descriptor_cross_validate_results = json.loads(file.read())
        if os.path.exists(os.path.join(invariance_path, metric_filename)):
            with open(os.path.join(invariance_path, metric_filename), 'r') as file:
                descriptor_invariance_results = json.loads(file.read())

        for descriptor_shape in descriptor_shapes:
            descriptor_metric = {}
            descriptor_metric["descriptor_name"] = descriptor_name
            descriptor_metric["descriptor_shape"] = descriptor_shape

            # Collect results
            if descriptor_cross_validate_results is not None:
                descriptor_metric['accuracy_mean'] = descriptor_cross_validate_results[f"descriptor_shape_{descriptor_shape}"]['accuracy_mean']
                descriptor_metric['accuracy_var'] = descriptor_cross_validate_results[f"descriptor_shape_{descriptor_shape}"]['accuracy_var']
                descriptor_metric['f1_macro_mean'] = descriptor_cross_validate_results[f"descriptor_shape_{descriptor_shape}"]['f1_macro_mean']
                descriptor_metric['f1_macro_var'] = descriptor_cross_validate_results[f"descriptor_shape_{descriptor_shape}"]['f1_macro_var']
            if descriptor_invariance_results is not None:
                for transform in ['shift', 'rotate', 'scale']:
                    descriptor_metric[f'{transform}_accuracy'] = descriptor_invariance_results[f"descriptor_{descriptor_shape}"][f'{transform}_accuracy']
                    descriptor_metric[f'{transform}_f1_macro'] = descriptor_invariance_results[f"descriptor_{descriptor_shape}"][f'{transform}_f1_macro']

            metrics.append(descriptor_metric)

    logger.info("Save metric results")
    metrics_dataframe = pandas.DataFrame(metrics)
    metrics_dataframe.to_csv(metric_path)

    logger.info("Finish metric composition")


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--config', default="/media/ubuntu/DATA/Projects/Personal/leaf-curve-modeling/params.yaml", help='path to yaml file with params')

    args = parser.parse_args()
    metric_composition(config_path=args.config)
