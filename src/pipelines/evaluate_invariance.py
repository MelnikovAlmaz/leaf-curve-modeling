import argparse
import json
import os
import pickle
import mlflow
import numpy
import yaml
from sklearn.metrics import accuracy_score, f1_score
from sklearn.model_selection import cross_validate
from sklearn.neighbors import KNeighborsClassifier

from src.features.ccod_descriptor import center_coordinate_distance_descriptor
from src.features.fourier_features import coordinates_fourier_descriptor, polar_coordinates_fourier_descriptor
from src.features.wavelet_features import coordinates_wavelet_descriptor, polar_coordinates_wavelet_descriptor
from src.utils.logging import get_logger


def evaluate_invariance(config_path: str) -> None:
    """
    Detect contour on image.
    Args:
        config_path: path to yaml file with params

    Returns: None

    """
    # Params
    config = yaml.safe_load(open(config_path, 'r'))
    log_level = config['base']['log_level']
    project_path = config['base']['project_path']

    coordinates_path = os.path.join(project_path, config['evaluate_invariance']['coordinates_path'])
    target_path = os.path.join(project_path, config['evaluate_invariance']['target_path'])

    shift_coordinates_path = os.path.join(project_path, config['evaluate_invariance']['shift_coordinates_path'])
    shift_target_path = os.path.join(project_path, config['evaluate_invariance']['shift_target_path'])

    rotate_coordinates_path = os.path.join(project_path, config['evaluate_invariance']['rotate_coordinates_path'])
    rotate_target_path = os.path.join(project_path, config['evaluate_invariance']['rotate_target_path'])

    scale_coordinates_path = os.path.join(project_path, config['evaluate_invariance']['scale_coordinates_path'])
    scale_target_path = os.path.join(project_path, config['evaluate_invariance']['scale_target_path'])

    descriptor_name = config['evaluate_invariance']['descriptor_name']
    descriptor_shapes = config['evaluate_invariance']['descriptor_shapes']
    n_neighbors = config['evaluate_invariance']['n_neighbors']

    result_path = os.path.join(project_path, config['evaluate_invariance']['result_path'])
    experiment_id = config['evaluate_invariance']['experiment_id']

    transform_types = ['origin', 'shift', 'rotate', 'scale']
    coordinates_path_dict = {
        'origin': coordinates_path,
        'shift': shift_coordinates_path,
        'rotate': rotate_coordinates_path,
        'scale': scale_coordinates_path
    }
    target_path_dict = {
        'origin': target_path,
        'shift': shift_target_path,
        'rotate': rotate_target_path,
        'scale': scale_target_path
    }
    coordinate_dict = {}
    target_dict = {}

    logger = get_logger("EVALUATE INVARIANCE", log_level)
    logger.info(f"Evaluate invariance")

    for transform in transform_types:
        logger.info(f"Load {transform} coordinates")
        with open(coordinates_path_dict[transform], 'rb') as file:
            coordinate_dict[transform] = pickle.load(file)
        logger.info(f"Load {transform} coordinates target")
        with open(target_path_dict[transform], 'rb') as file:
            target_dict[transform] = pickle.load(file)

    results = {}
    for descriptor_shape in descriptor_shapes:
        logger.info(f"Evaluate invariance for descriptor {descriptor_name} with shape {descriptor_shape}")
        logger.info(f"Prepare features")
        results[f"descriptor_{descriptor_shape}"] = {}

        descriptor_dict = {}
        for transform in transform_types:
            descriptor_list = []
            for coordinates in coordinate_dict[transform]:
                if descriptor_name == "ccod":
                    descriptor = center_coordinate_distance_descriptor(coordinates, descriptor_shape)
                if descriptor_name == "fourier_coordinates":
                    descriptor = coordinates_fourier_descriptor(coordinates, descriptor_shape)
                if descriptor_name == "wavelet_coordinates":
                    descriptor = coordinates_wavelet_descriptor(coordinates, descriptor_shape)
                if descriptor_name == "fourier_polar_coordinates":
                    descriptor = polar_coordinates_fourier_descriptor(coordinates, descriptor_shape)
                if descriptor_name == "wavelet_polar_coordinates":
                    descriptor = polar_coordinates_wavelet_descriptor(coordinates, descriptor_shape)
                descriptor_list.append(descriptor)
            descriptor_dict[transform] = descriptor_list

        # Trace metrics at mlflow
        with mlflow.start_run(experiment_id=experiment_id):
            logger.info(f"Fit model")
            mlflow.log_param("descriptor_shape", descriptor_shape)
            mlflow.log_param("descriptor_name", descriptor_name)

            model = KNeighborsClassifier(n_neighbors=n_neighbors)
            model.fit(descriptor_dict['origin'], target_dict['origin'])

            for transform in transform_types[1:]:
                logger.info(f"Test {transform} transform")
                predict = model.predict(descriptor_dict[transform])

                accuracy = accuracy_score(target_dict[transform], predict)
                f1_macro = f1_score(target_dict[transform], predict, average="macro")

                results[f"descriptor_{descriptor_shape}"][f'{transform}_accuracy'] = accuracy
                results[f"descriptor_{descriptor_shape}"][f'{transform}_f1_macro'] = f1_macro

                mlflow.log_metric(f'{transform}_accuracy', accuracy)
                mlflow.log_metric(f'{transform}_f1_macro', f1_macro)

    metric_filename = f"{descriptor_name}.json"
    with open(os.path.join(result_path, metric_filename), 'w') as file:
        file.write(json.dumps(results, indent=experiment_id))

    logger.info("Finish evaluate invariance")


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--config', default="/media/ubuntu/DATA/Projects/Personal/leaf-curve-modeling/params.yaml", help='path to yaml file with params')

    args = parser.parse_args()
    evaluate_invariance(config_path=args.config)
