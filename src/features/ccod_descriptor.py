from src.data.image_process import normalize_position, cart2pol
from src.data.vector_utils import interpolate_vector


def center_coordinate_distance_descriptor(coordinates, feature_size):
    center_coordinates = normalize_position(coordinates)
    (distance, angle) = cart2pol(center_coordinates.copy())
    # Normalize
    distance = distance / sum(distance)
    # Scale
    descriptor = interpolate_vector(distance, target_size=feature_size)

    return descriptor
