import numpy

from src.data.image_process import normalize_position, cart2pol


def coordinates_fourier_descriptor(coordinates_vector, descriptor_shape, is_sample_points=True):
    coordinates_vector = normalize_position(coordinates_vector)
    (distance, angle) = cart2pol(coordinates_vector.copy())
    coordinates_vector /= max(distance)

    if is_sample_points:
        coordinates_vector = coordinates_vector[::len(coordinates_vector) // descriptor_shape][:descriptor_shape]

    # Prepare complex number
    complex_coords = numpy.empty(shape=coordinates_vector.shape[0], dtype=numpy.complex)
    complex_coords.real = coordinates_vector[:, 0]
    complex_coords.imag = coordinates_vector[:, 1]

    descriptor = numpy.fft.fft(complex_coords)
    descriptor = numpy.abs(descriptor)

    if not is_sample_points:
        descriptor = descriptor[:descriptor_shape]

    return descriptor


def polar_coordinates_fourier_descriptor(coordinates_vector, descriptor_shape, is_sample_points=True):
    coordinates_vector = normalize_position(coordinates_vector)
    (distance, angle) = cart2pol(coordinates_vector.copy())
    distance /= max(distance)
    angle /= numpy.pi

    if is_sample_points:
        distance = distance[::len(distance) // descriptor_shape][:descriptor_shape]
        angle = angle[::len(angle) // descriptor_shape][:descriptor_shape]

    # Prepare complex number
    complex_coords = numpy.empty(shape=distance.shape[0], dtype=numpy.complex)
    complex_coords.real = distance
    complex_coords.imag = angle

    descriptor = numpy.fft.fft(complex_coords)
    descriptor = numpy.abs(descriptor)

    if not is_sample_points:
        descriptor = descriptor[:descriptor_shape]

    return descriptor
