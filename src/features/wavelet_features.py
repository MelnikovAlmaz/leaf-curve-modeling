import numpy
import pywt

from src.data.image_process import normalize_position, cart2pol


def coordinates_wavelet_descriptor(coordinates_vector, descriptor_shape, is_sample_points=True):
    coordinates_vector = normalize_position(coordinates_vector)
    (distance, angle) = cart2pol(coordinates_vector.copy())
    coordinates_vector /= max(distance)

    if is_sample_points:
        coordinates_vector = coordinates_vector[::len(coordinates_vector) // descriptor_shape][:descriptor_shape]

    yA, yD = pywt.dwt(coordinates_vector[:, 0], 'db1')
    xA, xD = pywt.dwt(coordinates_vector[:, 1], 'db1')

    if not is_sample_points:
        yA = yA[:descriptor_shape//2]
        xA = xA[:descriptor_shape//2]

    descriptor = numpy.hstack([yA, xA])

    return descriptor


def polar_coordinates_wavelet_descriptor(coordinates_vector, descriptor_shape, is_sample_points=True):
    coordinates_vector = normalize_position(coordinates_vector)
    (distance, angle) = cart2pol(coordinates_vector.copy())
    distance /= max(distance)
    angle /= numpy.pi

    if is_sample_points:
        distance = distance[::len(distance) // descriptor_shape][:descriptor_shape]
        angle = angle[::len(angle) // descriptor_shape][:descriptor_shape]

    yA, yD = pywt.dwt(distance, 'db1')
    xA, xD = pywt.dwt(angle, 'db1')

    if not is_sample_points:
        yA = yA[:descriptor_shape//2]
        xA = xA[:descriptor_shape//2]

    descriptor = numpy.hstack([yA, xA])

    return descriptor
