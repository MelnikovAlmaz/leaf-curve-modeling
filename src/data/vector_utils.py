import cv2
import numpy
from scipy.signal import find_peaks


def interpolate_vector(vector, target_size):
    depth = 1 if len(vector.shape) == 1 else vector.shape[1]

    new_vector = vector.copy().astype(float)
    new_vector = numpy.reshape(new_vector, (1,new_vector.shape[0], depth))
    new_vector = cv2.resize(new_vector, (target_size, 1))[0]
    return new_vector


def vector_extremum_indexes(vector):
    maxima_indexes = find_peaks(vector)[0]
    minima_indexes = find_peaks(vector * -1)[0]
    return numpy.hstack([maxima_indexes, minima_indexes])


def max_normalize_vector(vector):
    normalized_vector = vector.copy() / vector.max()
    return normalized_vector, vector.max()


def rotate_vector(vector):
    vector = vector - vector[0]
    m = (vector[-1][1] - vector[0][1]) / (vector[-1][0] - vector[0][0])
    theta = numpy.arctan(m)

    c, s = numpy.cos(theta), numpy.sin(theta)
    rx, ry = vector[0][0], vector[0][1]

    R = numpy.array(
        (
            (c, -s, rx*(1 - c) + ry*s),
            (s, c, ry*(1 - c) - rx*s),
            (0, 0, 1)
        )
    )

    vector = numpy.hstack([vector, numpy.ones(shape=(len(vector), 1))])
    vector = vector.T

    result = numpy.matmul(R, vector)[:2]
    result = result.T
    return result


def rotate(vector, origin, angle):
    R = numpy.array([[numpy.cos(angle), -numpy.sin(angle)],
                  [numpy.sin(angle),  numpy.cos(angle)]])
    o = numpy.atleast_2d(origin.copy())
    p = numpy.atleast_2d(vector.copy())

    return numpy.squeeze((R @ (p.T-o.T) + o.T).T)