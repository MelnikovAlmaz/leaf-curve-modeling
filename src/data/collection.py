import os


def collect_leaf_info(dataset_dir, class_number=100):
    image_path_list = []
    image_class_list = []

    for leaf_class in os.listdir(dataset_dir)[:class_number]:
        leaf_class_dir = os.path.join(dataset_dir, leaf_class)
        for image_name in os.listdir(leaf_class_dir):
            image_path = os.path.join(leaf_class_dir, image_name)

            image_path_list.append(image_path)
            image_class_list.append(leaf_class)

    return image_path_list, image_class_list
