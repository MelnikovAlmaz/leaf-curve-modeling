import cv2
import numpy

from src.remove_background.rm_background import remove_background


def load_image(image_path):
    image = cv2.imread(image_path, cv2.IMREAD_UNCHANGED)
    image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
    return image


def object_contour_detection(image, remove_bg=False):
    if remove_bg:
        image = remove_background(image)[:,:,3]
    contour_image = cv2.Canny(image, 50, 150)
    return contour_image


def contour_pixel_coordinates(contour_image):
    smoothed_image = cv2.GaussianBlur(contour_image, ksize=(3, 3), sigmaX=1)
    contour = cv2.findContours(smoothed_image, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)[0]
    contour = numpy.vstack(contour)
    coordinates = numpy.reshape(contour, (contour.shape[0], 2))
    return coordinates


def normalize_position(coordinates):
    origin = numpy.mean(coordinates, axis=0)
    normalized_coordinates = coordinates - origin
    return normalized_coordinates


def cart2pol(coordinates):
    rho = numpy.linalg.norm(coordinates, axis=1)
    phi = numpy.arctan2(coordinates[:,0], coordinates[:, 1])
    return rho, phi
