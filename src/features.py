import cv2
import numpy
from geomdl import fitting
import matplotlib.pyplot as plt
from scipy.signal import medfilt, find_peaks

from src.data.image_process import rotate


def interpolate_vector(vector, target_size):
    depth = 1 if len(vector.shape) == 1 else vector.shape[1]

    new_vector = vector.copy()
    new_vector = numpy.reshape(new_vector, (1,new_vector.shape[0], depth))
    new_vector = cv2.resize(new_vector, (target_size, 1))[0]
    return new_vector


def vector_extremum_indexes(vector):
    maxima_indexes = find_peaks(vector)[0]
    minima_indexes = find_peaks(vector * -1)[0]
    return numpy.hstack([maxima_indexes, minima_indexes])


def control_points_from_data_points(data_points, segment_number, control_point_per_segment, spline_degree, has_extreme_points=False, is_rotate=True, verbose=0):
    interval_indexes = list(range(0, len(data_points)+1, len(data_points) // segment_number))

    segment_evaluated_point_list = []
    segment_control_point_list = []

    for i in range(segment_number):
        # Select segment points
        segment_points = data_points[interval_indexes[i]: interval_indexes[i + 1]]

        # Rotate curve to make first and last point on same vertical level
        segment_origin = segment_points[0]
        angle = -numpy.arctan2(segment_points[-1][1] - segment_points[0][1], segment_points[-1][0] - segment_points[0][0])

        if is_rotate:
            segment_points = rotate(segment_points, segment_origin, angle)

        # Fit BISpline
        curve = fitting.approximate_curve(segment_points.tolist(), degree=spline_degree, ctrlpts_size=control_point_per_segment)
        curve.delta = 1 / len(segment_points)

        evaluated_points = numpy.array(curve.evalpts)
        control_points = numpy.array(curve.ctrlpts)

        if has_extreme_points:
            extremum_points = segment_points[vector_extremum_indexes(segment_points[:,1])]
            control_points = numpy.vstack([control_points, extremum_points])

        if verbose == 1:
            plt.plot(segment_points[:, 0], segment_points[:, 1])
            plt.plot(evaluated_points[:, 0], evaluated_points[:, 1])
            if has_extreme_points:
                plt.plot(extremum_points[:, 0], extremum_points[:, 1], 'x')
            plt.plot(control_points[:, 0], control_points[:, 1], 'o')
            plt.show()

        # Rotate curve points to data_points
        if is_rotate:
            evaluated_points = rotate(evaluated_points, segment_origin, -angle)
            control_points = rotate(control_points, segment_origin, -angle)

        segment_evaluated_point_list.append(evaluated_points.tolist())
        segment_control_point_list.append(control_points.tolist())

    segment_evaluated_point_list = numpy.array(segment_evaluated_point_list)
    segment_control_point_list = numpy.array(segment_control_point_list)

    return segment_evaluated_point_list, segment_control_point_list, curve
